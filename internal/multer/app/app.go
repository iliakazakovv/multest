package app

import (
	"mulTest/internal/multer/config"
	"mulTest/pkg/httpclient"
)

type Application struct {
	*config.Config
	HTTPClient httpclient.Client
}

func New(cfg *config.Config) *Application {
	return &Application{
		Config:     cfg,
		HTTPClient: httpclient.New(cfg.HTTPClientConfig),
	}
}
